import { Component, OnInit } from "@angular/core";
import Chart from "chart.js";
import axios from "axios";
import { environment } from "src/environments/environment";
import { DataFace, Data_lpr, lpr_detail } from "src/app/models/trackFace.model";

const dateFormat = require("dateformat");

@Component({
  selector: "app-lpr",
  templateUrl: "./lpr.component.html",
})
export class LprComponent implements OnInit {
  constructor() {}
  path = environment.service_uri;
  //path = `http://192.168.1.40:3006`;
  date: string = "03-Mar-21";
  dateM: string = "Mar-21";
  dateY: string = "21";
  dateFrom: string = "01-01-2021";
  dateTo: string = "01-03-2021";
  person: number = 0;
  personM: number = 0;
  personY: number = 0;
  dataColor: number[];
  labelColor: string[];
  lprColor: any[];
  totalPerson: number = 0;
  MONTHS = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  blacklist: number = 0;
  whitelist: number = 0;
  earkData = [
    {
      _id: "60903da501be3323edcb2043",
      country: "Thailand",
      createdDate: new Date(),
      make: "Chevrolet",
      model: "Cruze",
      plateNumber: "1กค294",
      platePicture:
        "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAwAEgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDhL/8A4LPfDj43eGf+EU/bh/Ye/Zd+O2j6derqslzeeGlXxBqF4zNFcXFitxqlzNZamY5JVjNvd26xKzLJK+MHwTXLL/g3T+L2o/2hqf7Pfxn/AGYtdlhjv4fFnwq8cSafbaVf6hvMttZ+GNXvNT0SW20YuoWWUiO8T948a4CH96rr/gk7/wAE1UsZLRP2V/Bn2i1laKG8S91hNUn+zCW1Ml/qMV2lzeyzJmS6knkb7Tdf6TKpkVSvhHjz/gmd/wAE0vBml6hq/iL9mLwP/Yuj20t/q15rGsagbOx063Bku7q8n1LURDBZWsX725k3hkiVtis3B8fDcV4atXjg1epUiru1m1tvtpbq9bvuf0RxRgeAK0K7pZa6GIvLllTlywT6JpK9r66q/wCa/FK5/wCCfn/BF3xZffbfhB/wVu+Jnwn8SSf2fMZvi/4L8G/EKxurM2UjSW0Fn4Jk8NPp139qjXFncstvBDvtZZJbkLM3L3H/AARu8G+Nf7fv/hP/AMFfP2NvF1lZBLzT5PiF4M8WfCu41i3uPs6QxW8OqeJHtzKZZWaZdOMiRxxSTRieNjK/7D+KP2Hv+CLnge98Mad8UP2dfgz8Pp/G+lTax4SsdXvtU0rVfEunxJBI99pdrf3vlNpscV1aXM+rXL2thbW11b3U1zFBPE5PG/8AwTs/4I5/DaHwxf638JPhz4Oh8c3ccHhV5/G+r21tr8cgja51XS4bS5vk/sKwNxbxXeuTRW+jbp7YWV/dRXVrJP6cs1lRXNGEuV2a066fN/PpfQ/Ea2QYCcpyVeKTl7qvbS6srNXvZdD8RH/4Ilftg+XEfDH7TP8AwTW8ZxzTiMX1l+1B4X8P28rjKW4uLe+027nWe7Xb5UHmPL57tuVGJFemaX/wQy/afvojpPxN/bl/4Jt/AdLCwj1HUra4+OeleMbn7OCouvNtozYzCUtOjWcFvYr/AGg/7qLyWBZv2107/gld/wAE0dXgWNv2a9Fu1ubq3i0q7j8V+MRb641wDJZTaY+l6lFPcRTvGzQeZZRvmJpSggxM3mWl/sUf8EfPE1z450TSfhD4C17UfhrZX+ofEC7Xxx4o1rR/Amm6W4gurvxdqUviUz2VisrfZ4LmKC5sHuDGjXiSMFaaOeTk5RdFxvtLlWuzfdWWi+e3VeXDLMHQqvlk5Ny+Jt26KyX+XXbc/J3RP+CQH7KWgPrGm/Hn/gtJ8B/CVrZK8lrJ8GPhJ4n1iS7gnkMOn3c2qa7Po9pOszLJJcWmmzX4QKIorwq63A7LwN+wZ/wQ28IWsuofET9vf9p74839rHvgsfDf/Crvh/ot3NaLvcLp1/ZeJ/EbwX0rxjZNehU/dor48x5f1Ot/+CX/APwSY1vwYfiFov7PPw71DwxJBLNNraeItbbTYoYYRd3E8k0Gt3lrDbxwtvmhunhu7eM5e0MQMlep/Db/AIJzf8Exb2w0668L/sk/CjVNKvIrSfTNZvNAjuYdWgvCwjvbO7ne4m1CCSZJALicrJIwyV2srV3QzGdVOUk404Ru9LbW8rXtrserhsHg41YxnNcsmm0nrd20s9Nd36L0X5W6T8U/+CFvwQuxcfBb/gnFZfFLxVaStajX/wBoLxl4h+IOnanEpmSNho4MGj2cdzESG022t4LBIEWOCyjWNcFfuov/AATR/YRt5YTY/sq/BmESEMI4vB+mKoOcYEiwo+RgZO8HqRjGAV4z4wyXnlD29SM6b5JrVe8muzXZ9uyt0+to5ZlUIR5oO7Sd4zavdxev/B20WyPvLV7XyLm7gkWNWhuZ4iEVQQRI4YE/xEHIGR1B6kivzo/4KJ/DW3+IX7L/AMffDu0yT3XwB+LuraVBHM8ckni3w54Q1XW/CfKEbxF4itNNlkgIcTRCSIghwD6F/wAE7UnP/BPf9hMMhKn9jX9mIqwBPyH4KeCSvI4OBxjqMFTyCB7j49sbW9utJhvLWG7tBb6pBdW88ayRXEd7DFA9vNGwKywTxs8cqMpDIWVhtY1+X8PzjR4gxt0pOFaVH3k0moytzJO6s7XWvTVvVmOc4+OKoVKrjJTqQUuVO/K6kVK19L2vvZNtapXs/wA/fhnpvhXx5+1X/wAE2NV+ImjaR4i0Px9/wRs+JWj3Ees6euoR2Piay1P4I6Fq0zbkeF9an0Kw1TTWguHM0di2qOY4jZXM1v8ABnwA+BXxX+JPiD/gjqR411DwPZfFn9gn9o3wb8YbjxTbtqEdvpX7N/xj12/s/CqtfCWLR9d8VeEtUGh3t4y3V1c2fh6GC3gje2glb9iPEXxU8M+AfFvh/wAPWfw7/tbV9K8GS2eianollbyXnh/StQv2hk8M6cqKTptjqEMklw1lGIzeM5YI8eSeDf8AbO/Zp8KiymuPD91pj+EElt7O0jsbc2+jReJpTFfWmiTuoRLLWLiWQ6jLo7yRyvdGS4cJJE7fr3O5UrqlF31tZW+ztZW/PzPz2NKpzOTrVLN3s5Suuvf/ACt6XT4f/gnBH4s1D9kz9nWHxLa6vY+IrPRPHPgi5tfE7Tx69p+maH8R/GnhewnvZLz/AE6w1MeGIID5jiK8sZC+EtpN0afG/hvSf2evj9pH7b2heFNVufgt8FvAHwB+MP7GXwj+H/hvw/qUHxH8UWFn4isPiZ8VPj1458RKkM/iHVLTxLd6F4d8D6Zq7XTyJYarPZyT2Fvc26fdUX7ef7Pkuv63odh4f8Y+Gr3w1d3KXVvdeHJbTRJbmYG9mXR70Qxi7luPtCzyNj9887O581pCex+H/wC1n8G9b0288TaP8ILiw0vTtRubHUtW1LwxZWey6m0ybULqe8MVsouLaXTY5rieeZnWSMHzNzE55uWqtVTiuuiStt2+fp8kZVak/axjCUr3invd6q76vp/mfnumn/EX41/sx/treF9I03SdY0b9oL9qT4HXF1J8ELY+FzcfAiX4E/D3SviHL8ObbXpY18PeJJbzwxqa+Io7pkFtq2v69BbNLctCkX3r+yhqek6z+zD+zOdG0TUtBHh34aab4fm0vVYVg1awu/DmuajpsGn6qwkc3Op2VvZxw6jKcCS58wjBbNbd7+0x8ONX0A3HgfwZF/Yk+paZJDHY6XFp+k6pdancQI02lWlnaRT3NzHZhp8WvmO5SIFAGIb3DTLfStJmSz0vS4LKyS+u2EMETRW4uLm5kvbx1jKhY5pZ53uZ0ABR5sFVyAHOu1h6lOaUW6c7PbZeS0Vt7P8AU78NQqyrwnNyfvR0vts9Fb+rPoe06W0jRQFmwzIncfeJ/LjHQkEAj0FFP09obpY/KIGGIIAYDO44IJGCMY7gk5PAor8Ax9T6rj8XF39+s5ptPa6StbTpfv16H6NQnhvZQ5pRvZaSvpaz018tfPyP5v8A9jT/AIL9/wDBO74M/sffsp/Bzxw3xufxr8LP2bPgd8NvFw0TwLBqGljxN4K+Gfhnw3rg025F3G1xYrqem3S207ANPFslIBc17bdf8HDX/BKzWtTtDqmvfH3QIw+1rq/+GMTWdsnzEyzRw3r3EgDbd0aAOVG4sACD/G18PvDHw/u/APgmfUb/AEW0vZfCHhyW4F1Z3Ik+0S6LZGUtLb5LOZC2XIwSCe+DR1D4IeF9Ylnks/FuhFHYtH5ty8YGeCo3rv288BskZGRiv16OU5esdKtBqM6lSU6kk7Pmum1239NLeh2VuBs6eGw9SFTDTVfDUKkIKcW7TpwlFSu9Gk1890f2iSf8F0/+CMtrNDcn4yfFbUZpra384L8Mr+CfyIXjaO3mvVKzx3kYTYJomLRRvKqSfNWb4c/4Lg/8EQtJ0/StNn8VeMtVj0i1itbSTxD8GDqLyJE/mq08qBS7eaqOOCECJGuFRQP4t/8Ahli3ukaWHWdAnUEAyDWzH7j92UByepxwPfIqq/7MOlw4SeeC55IY2OsZBxjoWTIBznp04969+OLpRiqcZK0Hy6u70aW9lv8A5ebXBDwv4oupyp0EppNctWLvdJ7J6aa+R/cFJ/wX8/4I2xxNb2vi/WkRSxUn4G30kr4JwXnMheWRuplclizE8BQBBZf8HBn/AASC/f6dN458aabZykGQN8FLqWxuS6PA2+2t7mNiDbsYmMobdCzIwwQtfxIWn7KOk3KM0FvfEAhSW8QxRAMegw6jjkdBx0xxVr/hjkn97LBpyxNhj9o8UW7TFeQTtLAZwDjpyBT+tw/mX9cv+fzv56c0vC/iTn5vq8U4639onf4dnovmj+3iT/gub/wRjCW3kfGDWIYltftttbaX8JdS01LSZQ5SE286MtregDaPIbYu5FEgANZtx/wX8/4JF6fsex+KXxAvWkRZzG3w5v3MbNu3LJscfvxgCXbkHI+YjkfxRr+yT4Qx5c/2W2mXGZh4htpFP8JwNxBycnjHboM1An7KXgWC5jNz4o8OW8A5k+0a9OJQATwEtUJbpwA3U81z1qlCsuVtX0W68n21Wvk356hS8PeJ41bOlGKbV71Fa2nmr/07n9qEv/ByZ/wSm0+Qwx6/8apjExVpbH4Ys6u8ZZC8HmXysyORuRnQMUI3AEkUV/F4P2Z/hyl2k8PjDwZFArFpPtl9qVy7bQ2MKoG4AkbcsDyGb0oryKuRZRiJ+0q0Yyk7Jt311X+fn92/sYbgLiJRkpzoxtKyTnF/y67r181fvp//2Q==",
      vehicleColor: "red",
      vehicleType: "car",
      year: 2021,
      month: 5,
      day: 3,
    },
  ];

  test: number;

  // * age and gender
  male: number = 0;
  female: number = 0;

  // * items
  glasses: number = 0;
  sunGlasses: number = 0;
  mask: number = 0;
  hat: number = 0;

  // * Hairstyle
  shaven: number = 0;
  baid: number = 0;
  buzzcut: number = 0;
  short: number = 0;
  medium: number = 0;
  tress: number = 0;
  snood: number = 0;
  unknown: number = 0;
  startDateV: any;
  endDateV: any;

  // * repositories
  normals: number[] = [];
  vip: number[] = [];
  //blacklist: number[] = [];

  //age 0-60
  age_5: number = 0;
  age_9: number = 0;
  age_19: number = 0;
  age_29: number = 0;
  age_39: number = 0;
  age_49: number = 0;
  age_59: number = 0;
  age_60: number = 0;
  dashLpr: any[];
  dataD: lpr_detail;
  ngOnInit() {
    var today = new Date();
    var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
    var lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);

    let startDate = new Date(today.getFullYear(), today.getMonth(), 1);
    startDate.setHours(0, 0, 0, 0);
    let endDate = new Date();
    endDate.setHours(23, 59, 59, 999);
    this.startDateV = startDate;
    this.endDateV = startDate;
    axios
      .get(
        this.path +
          `/api/v1/lpr/dashboard?startDate=` +
          +startDate +
          `&endDate=` +
          +endDate
      )
      .then((res: any) => {
        //this.dataD = res.data.data;
        let dataLpr: Data_lpr[] = res.data.data;
        this.dashLpr = dataLpr;
        console.log("start filter");
        today = new Date();
        const date = today.getDate();
        const mm = today.getMonth(); //January is 0!
        const yyyy = today.getFullYear();
        this.person = dataLpr.filter(
          (w) =>
            new Date(w.createdDate).getDate() == date &&
            new Date(w.createdDate).getMonth() == mm &&
            new Date(w.createdDate).getFullYear() == yyyy
        ).length;

        this.personM = dataLpr.filter(
          (w) =>
            new Date(w.createdDate).getMonth() == mm &&
            new Date(w.createdDate).getFullYear() == yyyy
        ).length;

        let distinctMake = dataLpr
          .filter(
            (thing, i, arr) => arr.findIndex((t) => t.make === thing.make) === i
          )
          .map(({ make }) => make);
        let distinctColor = dataLpr
          .filter(
            (thing, i, arr) =>
              arr.findIndex((t) => t.vehicleColor === thing.vehicleColor) === i
          )
          .map(({ vehicleColor }) => vehicleColor);
        console.log(distinctColor);
        let distinctType = dataLpr
          .filter(
            (thing, i, arr) =>
              arr.findIndex((t) => t.vehicleType === thing.vehicleType) === i
          )
          .map(({ vehicleType }) => vehicleType);
        this.initTotalChart(dataLpr);
        this.initBrandlChart(dataLpr, distinctMake);
        this.initTypesChart(dataLpr, distinctType);
        this.initColorChart(dataLpr, distinctColor);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, "0");
        // var mm = today.getMonth(); //January is 0!
        // var yyyy = today.getFullYear();
        var today = new Date();
        this.date = dd + "-" + this.MONTHS[mm] + "-" + yyyy;
        this.dateM = this.MONTHS[mm] + "-" + yyyy;
        this.dateY = yyyy.toString();
        var firstDay = new Date(
          today.getFullYear(),
          today.getMonth(),
          1
        ).getDate();
        var lastDay = new Date(
          today.getFullYear(),
          today.getMonth() + 1,
          0
        ).getDate();
        this.dateFrom =
          firstDay.toString() + "-" + this.MONTHS[mm] + "-" + yyyy;
        this.dateTo = lastDay.toString() + "-" + this.MONTHS[mm] + "-" + yyyy;

        this.totalPerson = dataLpr.length;
        this.blacklist = 0;
        this.whitelist = 0;
      })
      .catch((err) => {
        console.log(err.message);
      });

    var firstYear = new Date(today.getFullYear(), 0, 1);
    var lastYear = new Date(today.getFullYear(), 11, 31);
    console.log(+firstYear);
    console.log(+lastYear);
    axios
      .get(
        this.path +
          `/api/v1/lpr/dashboard?startDate=` +
          +firstYear +
          `&endDate=` +
          +lastYear
      )
      .then((res: any) => {
        //this.dataD = res.data.data;
        let dataLpr: Data_lpr[] = res.data.data;
        this.dashLpr = dataLpr;
        console.log(dataLpr);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = today.getMonth(); //January is 0!
        var yyyy = today.getFullYear();
        var today = new Date();
        // this.person = dataLpr.filter(
        //   (w) =>
        //     new Date(w.createdDate).getDate() == today.getDate() &&
        //     new Date(w.createdDate).getMonth() == mm &&
        //     new Date(w.createdDate).getFullYear() == yyyy
        // ).length;
        // this.personM = dataLpr.filter(
        //   (w) =>
        //     new Date(w.createdDate).getMonth() == mm &&
        //     new Date(w.createdDate).getFullYear() == yyyy
        // ).length;

        this.personY = dataLpr.filter(
          (w) => new Date(w.createdDate).getFullYear() == yyyy
        ).length;
      })
      .catch((err) => {
        console.log(err.message);
      });

    axios
      .get(
        this.path +
          `/api/v1/faceImage?startDate=` +
          +startDate +
          `&endDate=` +
          endDate
      )
      .then((res: any) => {
        let dataFace: DataFace[] = res.data.data;
        this.blacklist = dataFace.filter((w) => w.personType.id == 1).length;
        this.whitelist = dataFace.filter((w) => w.personType.id != 1).length;
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  search(e) {
    let startDate: number = +e.startDate;
    let endDate: number = +e.endDate;

    this.callService(startDate, endDate);
  }

  async callService(startDate: number, endDate: number) {
    axios
      .get(
        this.path +
          `/api/v1/lpr/dashboard?startDate=` +
          startDate +
          `&endDate=` +
          endDate //+
        //`&page=1&limit=10`
      )
      .then((res: any) => {
        let dataLpr: Data_lpr[] = res.data.data;
        this.dashLpr = dataLpr;
        console.log(dataLpr);
        let distinctMake = dataLpr
          .filter(
            (thing, i, arr) => arr.findIndex((t) => t.make === thing.make) === i
          )
          .map(({ make }) => make);
        let distinctColor = dataLpr
          .filter(
            (thing, i, arr) =>
              arr.findIndex((t) => t.vehicleColor === thing.vehicleColor) === i
          )
          .map(({ vehicleColor }) => vehicleColor);
        let distinctType = dataLpr
          .filter(
            (thing, i, arr) =>
              arr.findIndex((t) => t.vehicleType === thing.vehicleType) === i
          )
          .map(({ vehicleType }) => vehicleType);
        this.initTotalChart(dataLpr);
        this.initBrandlChart(dataLpr, distinctMake);
        this.initColorChart(dataLpr, distinctColor);
        this.initTypesChart(dataLpr, distinctType);
        var today = new Date(startDate);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = today.getMonth(); //January is 0!
        var yyyy = today.getFullYear();
        var fromday = new Date(endDate);
        var fdd = String(today.getDate()).padStart(2, "0");
        var fmm = today.getMonth(); //January is 0!
        var fyyyy = today.getFullYear();

        this.dateFrom = dateFormat(new Date(startDate), "dd-mmm-yyyy");
        this.dateTo = dateFormat(new Date(endDate), "dd-mmm-yyyy");

        this.totalPerson = dataLpr.length;
      })
      .catch((err) => {
        console.log(err.message);
      });
    axios
      .get(
        this.path +
          `/api/v1/faceImage?startDate=` +
          startDate +
          `&endDate=` +
          endDate
      )
      .then((res: any) => {
        let dataFace: DataFace[] = res.data.data;
        this.blacklist = dataFace.filter((w) => w.personType.id == 1).length;
        this.whitelist = dataFace.filter((w) => w.personType.id != 1).length;
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  initTotalChart(lpr: Data_lpr[]) {
    new Chart("totalChart", {
      type: "bar",
      data: {
        labels: this.MONTHS,
        datasets: [
          {
            label: "Populations",
            data: [
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 1)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 2)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 3)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 4)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 5)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 6)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 7)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 8)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 9)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 10)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 11)
                .length,
              lpr.filter((w) => new Date(w.createdDate).getMonth() + 1 == 12)
                .length,
            ],
            backgroundColor: [
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
              "#03989E",
            ],
            hoverBackgroundColor: ["#66A2EB", "#FCCE56"],
          },
        ],
      },
      options: {
        responsive: true,
        scales: {
          xAxes: [
            {
              ticks: {
                min: 0,
              },
            },
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Years",
              },
            },
          ],
        },
        legend: {
          display: false,
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            },
          },
        },
      },
    });
  }

  initBrandlChart(lpr: Data_lpr[], ban: string[]) {
    let data = [];
    let label = [];

    for (let index = 0; index < ban.length; index++) {
      label.push(ban[index]);
      data.push(lpr.filter((w) => w.make == ban[index]).length);
    }
    new Chart("brandlChart", {
      type: "bar",
      data: {
        labels: label,
        datasets: [
          {
            label: "Populations",
            data: data,
            backgroundColor: [
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
              "#CB6CE6",
            ],
            hoverBackgroundColor: ["#66A2EB", "#FCCE56"],
          },
        ],
      },
      options: {
        responsive: true,
        scales: {
          xAxes: [
            {
              ticks: {
                min: 0,
              },
            },
          ],

          yAxes: [
            {
              ticks: {
                min: 0,
                stepSize: 1,
              },
              scaleLabel: {
                display: true,
                //labelString: "Years",
              },
            },
          ],
        },
        legend: {
          display: false,
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            },
          },
        },
      },
    });
  }

  initColorChart(lpr: Data_lpr[], color: string[]) {
    let data = [];
    for (let index = 0; index < color.length; index++) {
      //this.label.push(color[index]);
      data.push(lpr.filter((w) => w.vehicleColor == color[index]).length);
    }
    this.labelColor = color;
    this.dataColor = data;

    let no = 1;
    let obj: any[] = [];
    for (const result of color) {
      obj.push({
        no: no++,
        color: result,
        count: lpr.filter((w) => w.vehicleColor == result).length,
      });
    }
    this.lprColor = obj;
    // colorChart
    // new Chart("colorChart", {
    //   type: "pie",
    //   data: {
    //     labels: label,
    //     datasets: [
    //       {
    //         label: "Populations",
    //         data: data,
    //         backgroundColor: label,
    //         // [
    //         //   "Black",
    //         //   "#FFFFFF",
    //         //   "#D7D7D7",
    //         //   "#7E7E7E",
    //         //   "#0070BF",
    //         //   "#FF0000",
    //         //   "#FFD766",
    //         //   "#9D480E",
    //         //   "#91CF50",
    //         //   "#FF9797",
    //         // ],
    //         hoverBackgroundColor: ["#66A2EB", "#FCCE56"],
    //       },
    //     ],
    //   },
    //   options: {
    //     responsive: true,
    //   },
    // });
  }

  initTypesChart(lpr: Data_lpr[], type: string[]) {
    let data = [];
    let label = [];

    for (let index = 0; index < type.length; index++) {
      console.log(type[index]);
      if (!type[index]) {
        label.push("Unknown");
      }

      if (type[index]) {
        label.push(type[index]);
      }

      data.push(lpr.filter((w) => w.vehicleType == type[index]).length);
    }
    // typeChart
    new Chart("typesChart", {
      type: "doughnut",
      data: {
        labels: label,
        datasets: [
          {
            label: "Populations",
            data: data,
            backgroundColor: [
              "#5271FF",
              "#A978F1",
              "#D786E1",
              "#F29ED6",
              "#FFBBD4",
              "#C995BD",
            ],
            hoverBackgroundColor: ["#66A2EB", "#FCCE56"],
          },
        ],
      },
      options: {
        responsive: true,
      },
    });
  }
}
